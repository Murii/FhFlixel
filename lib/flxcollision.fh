
# TODO: 
# 1) Add a hash partition algorith for having less collision checks

fn collision() {
	let self = {};

	let gravityx = 0;
	let gravityy = 0;

	let shape = shape();

	let nchecks = 0;

	let pi = math_pi();
	let pi2 = pi * 2;
	let atan2 = math_atan2;
	let cos = math_cos;

	self.changePosition = fn(a, vx, vy, dt) {
		a.x = a.x + vx * dt;
		a.y = a.y + vy * dt;
	};

	self.calculateFriction = fn(d, dt) {
		let _minus_x = d.friction.x * dt;
		if (d.xv > 0.0 ){
			d.xv = d.xv - _minus_x; if (d.xv < 0.0 ){ d.xv = 0.0; }
		} elif (d.xv < 0.0 ){
			d.xv = d.xv + _minus_x; if (d.xv > 0.0 ){ d.xv = 0.0; }
		}
		let _minus_y = d.friction.y * dt;
		if (d.yv > 0.0 ){
			d.yv = d.yv - _minus_y; if (d.yv < 0.0 ){ d.yv = 0.0; }
		} elif (d.yv < 0.0 ){
			d.yv = d.yv + _minus_y; if (d.yv > 0.0 ){ d.yv = 0.0; }
		}
	};

	# resolves collisions
	self.solveCollision = fn(a, b, nx, ny, pen, dt) {
		# shape a must be dynamic
		#assert(a.list == dynamics, "collision pair error")
		let vx = a.xv - b.xv;
		let vy = a.yv - b.yv;
		let dp = vx*nx + vy*ny;

        if (dp > 0) {
            return;
        }

		# objects moving towards each other?
		if (dp < 0 ){
			# find separation velocity
			# project velocity onto the collision normal
			let sx = nx*dp; 
			let sy = ny*dp;
			# find collision impulse
			let r = 1 + a.bounce;

			let dvx = sx*r;
			let dvy = sy*r;
			# adjust the velocity of shape a
			a.xv = a.xv - dvx;
			a.yv = a.yv - dvy;
			if (b.isDynamic == true) {
				let arc = atan2(vy, vx) - atan2(ny, nx);
				arc = (arc + pi) % pi2 - pi;
				#assert(arc <= pi and arc >= -pi, "collision angle error")
				let p = cos(arc);
				# adjust the velocity of shape b
				b.xv = b.xv - dvx*p;
				b.yv = b.yv - dvy*p;
			}
		}

		# find the separation vector
		let sx = nx*pen;
		let sy = ny*pen;
		#assert(sx ~= 0 or sy ~= 0, "collision separation error")
		# store the separation for shape a
		a.sx = a.sx * sx;
		a.sy = a.sy * sy;

		# separate the pair by moving shape a
        self.changePosition(a, sx, sy, 1);
	};

	# check and report collisions
	self.checkCollision = fn(a, b, dt) {
		# track the number of collision checks (optional)
		self.nchecks = nchecks + 1;

		let test = shape.test(a, b, dt);
		if (!test) {
			return;
		}
		let nx = test[0];
		let ny = test[1];
		let pen = test[2];

		#assert(pen > 0, "collision penetration error")
		# collision callbacks
		let ra = true;
		let rb = true;
		if (a.onCollide ){
			ra = a.onCollide(a, b, nx, ny, pen);
		}
		if (b.onCollide ){
			rb = b.onCollide(b, a, -nx, -ny, pen);
		}
		# ignore collision if either callback returned false
		if ( ra == true && rb == true && a.allowCollision && b.allowCollision){
			self.solveCollision(a, b, nx, ny, pen, dt);
		}
	};

	self.clampVelocities = fn(d) {
		if (d.xv > d.maxVelocity.x ){
			d.xv = d.maxVelocity.x;
		}
		elif (d.xv < -d.maxVelocity.x ){
			d.xv = -d.maxVelocity.x;
		}
		if (d.yv > d.maxVelocity.y ){
			d.yv = d.maxVelocity.y;
		}
		elif (d.yv < -d.maxVelocity.y ){
			d.yv = -d.maxVelocity.y;
		}
	};

	self.collisionDraw = fn() {
		for (let i = 0; i < len(shape.getDynamics()); i = i + 1) {
			self.shape.drawDebug(shape.getDynamics()[i]);
		}
		for (let i = 0; i < len(shape.getStatic()); i = i + 1) {
			self.shape.drawDebug(shape.getStatic()[i]);
		}
	};

	let updateDynamic = fn(d, dt) {
		# damping
		let c = 1 + d.damping;
		let xv = d.xv/c;
		let yv = d.yv/c;
		# gravity
		let xg = d.gravity.x + gravityx;
		let yg = d.gravity.y + gravityy;
		if (d.allowGravity ){
			xv = xv + xg*dt;
			yv = yv + yg*dt;
		}
		d.xv = xv;
		d.yv = yv;
		self.clampVelocities(d);
		self.calculateFriction(d, dt);

		# reset separation
		d.sx = 0;
		d.sy = 0;
	};

	self.binary_search = fn(arr, first, last, x) {
		let mid = tointeger((first + last) * 0.5);
		while (first <= last) {
			if (arr[mid].id < x) {
				first = mid + 1;
			} else if (arr[mid].id == x) {
				return mid;
			} else {
				last = mid - 1;
			}
			mid = tointeger((first + last) * 0.5);
		}
		return -1;
	};

	# -------- PUBLIC FUNCTIONS -------- #
	return {
		'getSelf': fn() {
			return self;
		},
		'removeDynamic': fn(index) {
			let findIndexById = self.binary_search(shape.getDynamics(), 0, 
				len(shape.getDynamics()) - 1, index);
			if (findIndexById > -1) {
				delete(shape.getDynamics(), findIndexById);
			}
		},
		'removeStatic': fn(index) {
			let findIndexById = self.binary_search(shape.getStatic(), 0, 
				len(shape.getStatic()) - 1, index);
			if (findIndexById > -1) {
				delete(shape.getStatic(), findIndexById);
			}
		},
		# updates the simulation
		'update': fn(dt) {
			# track the number of collision checks (optional)
			self.nchecks = 0;
			let d_len = len(shape.getDynamics());
			let s_len = len(shape.getStatic());

			for (let i = 0; i < d_len; i = i + 1) {
				let d = shape.getDynamics()[i];
				updateDynamic(d, dt);

				for (let o = i + 1; o < d_len; o = o + 1) {
					self.checkCollision(d, shape.getDynamics()[o], dt);
				}
				for (let o = 0; o < s_len; o = o + 1) {
					self.checkCollision(d, shape.getStatic()[o], dt);
				}
			}
		}, #fn update()
		'changePosition': fn(a, vx, vy, dt) {
			return self.changePosition(a, vx, vy, dt);
		},
		'getShape': fn() {
			return shape;
		},
		'setGravity': fn(x, y) {
			gravityx = x;
			gravityy = y;
		},
		#-
		# Collision detection function;
		# Returns *true* if two boxes overlap, false if they don't;
		# x1,y1 are the top-left coords of the first box,
		# while w1,h1 are its width and height;
		# x2,y2,w2 & h2 are the same, but for the second box.
		-#
		'aabb': fn(x1, y1, w1, h1, x2, y2, w2, h2) {
			return x1 < x2+w2 &&
			x2 < x1+w1 &&
			y1 < y2+h2 &&
			y2 < y1+h1;
		},
	};
}
